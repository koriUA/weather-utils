//https://api.weather.com/v2/turbo/vt1observation?apiKey=c1ea9f47f6a88b9acb43aba7faf389d4&format=json&geocodes=22.65%2C88.34%3B14.95%2C120.90%3B39.29%2C-76.61%3B52.01%2C47.80%3B27.87%2C52.66%3B29.73%2C77.32%3B25.83%2C74.09%3B27.82%2C93.90%3B37.64%2C48.64%3B37.40%2C58.45&language=en-PW&units=m
//https://dsx.weather.com/x/v2/web/loc/en_PW/1/4/5/9/11/13/19/21/1000/1001/1003/pw%5E/gir?api=7bb1c920-7027-4289-9c96-ae5e263980bc&format=json&pg=0%2C10

var http = require('http');



function getLocationsBySearch(str) {
    return new Promise(function(resolve){
        var options = {
            host: 'dsx.weather.com',
            path: `/x/v2/web/loc/en_PW/1/4/5/9/11/13/19/21/1000/1001/1003/pw%5E/${str}?api=7bb1c920-7027-4289-9c96-ae5e263980bc&format=json&pg=0%2C10`
        };

        callback = function(response) {
            var str = '';

            response.on('data', function (chunk) {
                str += chunk;
            });

            response.on('end', function () {
                resolve(JSON.parse(str));
            });
        };

        http.request(options, callback).end();
    });

}

function getWeatherForcast(geocodesArr){
    var geocodesStr = encodeURIComponent(geocodesArr.join(';'));
    return new Promise(function(resolve){
        var options = {
            host: 'api.weather.com',
            path: `/v2/turbo/vt1observation?apiKey=c1ea9f47f6a88b9acb43aba7faf389d4&format=json&geocodes=${geocodesStr}&language=en-PW&units=m`
        };

        callback = function(response) {
            var str = '';

            response.on('data', function (chunk) {
                str += chunk;
            });

            response.on('end', function () {
                resolve(JSON.parse(str));
            });
        };

        http.request(options, callback).end();
    });

}



function randomString(length) {
    var chars = 'abcdefghijklmnopqrstuvwxyz';
    var result = '';
    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
    return result;
}



function search(str){
    getLocationsBySearch(str).then(function(data){
        if (!data.length){
            return;
        }
        var geocodesArr = [];
        data.forEach(el => {
            geocodesArr.push(el.geocode)
        });
        getWeatherForcast(geocodesArr).then(function(forecastData){
            if (!forecastData.length){
                return;
            }
            forecastData.forEach(el => {
                if (el.vt1observation.icon == '21' || el.vt1observation.icon == '19'|| el.vt1observation.icon == '20' || el.vt1observation.icon == '22'){
                    console.log('location:', el.id);
                }
            });
        })
    });
}


for(i = 1; i <=100; i++){
    search(randomString(3));
}